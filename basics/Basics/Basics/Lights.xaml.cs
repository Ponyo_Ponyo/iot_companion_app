using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Basics
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Lights : ContentPage
    {
        Client client = new Client(); //Creates new client

        string buttonState = "off"; //Stores on/off button state
        string IP; //Stores IP address
        string port; //Stores port
        string RED; //Stores red RGB value
        string GREEN; //Stores green RGB value
        string BLUE; //Stores blue RGB value

        public Lights()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false); //Turns off navigationbar on top of the app 
            IP = Preferences.Get("IP_key", ""); //Collects the saved IP address and stores it in  IP
            port = Preferences.Get("port_key", ""); //Collects the saved port and stores it in port
            RED = Preferences.Get("Reds", ""); //Colects the red RGB value and stores it in RED
            GREEN = Preferences.Get("Greens", ""); //Collects the green RGB value and stores it in GREEN
            BLUE = Preferences.Get("Blues", ""); //Collects the blue RGB value and stores it in BLUE
            buttonState = Preferences.Get("OnOffButton", "off"); //Collects on/off button state and stores it in buttonState
            if (buttonState == "on") //Checks if buttonState is on and changes button if true
            {
                LightSwitch.TextColor = Color.Black; //Changes text color to black 
                LightSwitch.BackgroundColor = Color.White; //Changes backgroundcolor to white
            }

        }

        public void Set() //Sets client IP address and endpoint
        {
            try
            {
                client.ipAddress = IPAddress.Parse(IP); //Sets client IP address to IP
                client.remoteEP = new IPEndPoint(client.ipAddress, Convert.ToInt32(port)); //Sets client endpoint to client IP address and port
            }
            catch
            {
            }
        }

        public void OpenConnection() //Opens connection with server and sets client IP address and endpoint
        {
            Set(); //Sets client IP address and endpoint
            client.Socket(); //Opens connection with server
        }

        protected void Connection(object sender, EventArgs e) //Navbar button that takes user to Status page 
        {
            Preferences.Set("OnOffButton", buttonState); //Sets on/off button state
            Navigation.PushAsync(new Status(client)); //Takes user to status page 
        }

        private void Settingss(object sender, EventArgs e) //Navbar button that takes user to Settings page
        {
            Preferences.Set("OnOffButton", buttonState); //Sets on/off button state
            Navigation.PushAsync(new Settings(client)); //Takes user to status page 
        }

        private void LightSwitchClicked(object sender, EventArgs e) //Light on/off button
        {
            if (buttonState == "on") //Checks wether button is on or off
            {
                OpenConnection(); //Opens connection with server and sets client IP address and endpoint
                client.Send("off"); //Sends a message to server telling it to turn the light off
                client.CloseConnection(); //Closes connection with server
                LightSwitch.TextColor = Color.Gray; //Changes text color to gray
                LightSwitch.BackgroundColor = Color.FromHex("303030"); //Changes background color to hex code value
                buttonState = "off"; //Sets on/off button state
            }
            else
            {
                OpenConnection(); //Opens connection with server and sets client IP address and endpoint
                client.Send("on"); //Sends a message to server telling it to turn the light on
                client.CloseConnection(); //Closes connection with server
                LightSwitch.TextColor = Color.Black; //Changes text color to black
                LightSwitch.BackgroundColor = Color.White; //Changes background color to white
                buttonState = "on"; //Sets on/off button state
            }
        }

        private void WhiteButton(object sender, EventArgs e)
        {
            OpenConnection(); //Opens connection with server and sets client IP address and endpoint
            client.Send("cChange,255,255,255"); //Sends a message to server telling it to turn the light to white
            client.CloseConnection(); //Closes connection with server
        }
        private void RedButton(object sender, EventArgs e)
        {
            OpenConnection(); //Opens connection with server and sets client IP address and endpoint
            client.Send("cChange,255,0,0"); //Sends a message to server telling it to turn the light to red
            client.CloseConnection(); //Closes connection with server
        }
        private void BlueButton(object sender, EventArgs e)
        {
            OpenConnection(); //Opens connection with server and sets client IP address and endpoint
            client.Send("cChange,0,0,255"); //Sends a message to server telling it to turn the light to blue
            client.CloseConnection(); //Closes connection with server
        }
        private void GreenButton(object sender, EventArgs e)
        {
            OpenConnection(); //Opens connection with server and sets client IP address and endpoint
            client.Send("cChange,0,255,0"); //Sends a message to server telling it to turn the light to green
            client.CloseConnection(); //Closes connection with server
        }
        private void OrangeButton(object sender, EventArgs e)
        {
            OpenConnection(); //Opens connection with server and sets client IP address and endpoint
            client.Send("cChange,255,165,0"); //Sends a message to server telling it to turn the light to orange
            client.CloseConnection(); //Closes connection with server
        }
        private void CyanButton(object sender, EventArgs e)
        {
            OpenConnection(); //Opens connection with server and sets client IP address and endpoint
            client.Send("cChange,0,255,255"); //Sends a message to server telling it to turn the light to cyan
            client.CloseConnection(); //Closes connection with server
        }
        private void PurpleButton(object sender, EventArgs e)
        {
            OpenConnection(); //Opens connection with server and sets client IP address and endpoint
            client.Send("cChange,128,0,128"); //Sends a message to server telling it to turn the light to purple
            client.CloseConnection(); //Closes connection with server
        }
        private void YellowButton(object sender, EventArgs e)
        {
            OpenConnection(); //Opens connection with server and sets client IP address and endpoint
            client.Send("cChange,255,255,0"); //Sends a message to server telling it to turn the light to yellow
            client.CloseConnection(); //Closes connection with server
        }
        private void CustomButton(object sender, EventArgs e)
        {
            OpenConnection(); //Opens connection with server and sets client IP address and endpoint
            client.Send("cChange," + RED + "," + GREEN + "," + BLUE); //Sends a message to server telling it to turn the light to the custom color
            client.CloseConnection(); //Closes connection with server
        }
    }
}
