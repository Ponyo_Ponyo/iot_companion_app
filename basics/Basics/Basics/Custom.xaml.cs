using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Basics
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Custom : ContentPage
    {
        Client client;

        public Custom(Client client)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            REDu.Text = Preferences.Get("Reds", "");
            GREENu.Text = Preferences.Get("Greens", "");
            BLUEu.Text = Preferences.Get("Blues", "");
            this.client = client;
        }

        async void SaveButton(object sender, EventArgs e)
        {
            try
            {
                int rValue = Convert.ToInt32(REDu.Text);
                int gValue = Convert.ToInt32(GREENu.Text);
                int bValue = Convert.ToInt32(BLUEu.Text);
                if (rValue == 0 && gValue == 0 && bValue == 0){await DisplayAlert("Error", "0,0,0 Isn't a valid RGB value", "OK");}
                else
                {
                    if (rValue > 255 || rValue < 0){REDu.Text = Convert.ToString(255);}
                    if (gValue > 255 || gValue < 0){GREENu.Text = Convert.ToString(255);}
                    if (bValue > 255 || bValue < 0){BLUEu.Text = Convert.ToString(255);}
                    Preferences.Set("Reds", REDu.Text);
                    Preferences.Set("Greens", GREENu.Text);
                    Preferences.Set("Blues", BLUEu.Text);
                }
            }
            catch{}
        }
        //Navigationbutton code
        protected void Connection(object sender, EventArgs e){Navigation.PushAsync(new Status(client));}
        private void Lampjes(object sender, EventArgs e){Navigation.PushAsync(new Lights());}
        private void Settingss(object sender, EventArgs e){Navigation.PushAsync(new Settings(client));}
    }
}