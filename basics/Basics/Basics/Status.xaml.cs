﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net;

namespace Basics
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Status : ContentPage
    {
        Client client = new Client();
        string port;
        string ip;
        public Status(Client client)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            ip = Preferences.Get("IP_key", "");
            port = Preferences.Get("port_key", "");
            this.client = client;
            if (ip.Length > 0)
            {
                try
                {
                    this.client.ipAddress = IPAddress.Parse(ip);
                    this.client.remoteEP = new IPEndPoint(this.client.ipAddress, Convert.ToInt32(port));
                    statusButton.IsEnabled = true;
                    IPAdress.Text = ip;
                    Port.Text = port;
                }
                catch (Exception){}
            }
            else{statusButton.IsEnabled = false;}
        }

        protected void OnStatus(object sender, EventArgs e)
        {
            try
            {
                IPAdress.Text = ip;
                Port.Text = port;
                LightStatus.Text = client.LightStatus();
                SensorStatus.Text = client.SensorStatus();
                TimerStatus.Text = client.TimerStatus();
            }
            catch (Exception){}
        }

        //Navigationbutton code
        private void Lampjes(object sender, EventArgs e){Navigation.PushAsync(new Lights());}
        private void Settingss(object sender, EventArgs e){Navigation.PushAsync(new Settings(client));}
    }
}