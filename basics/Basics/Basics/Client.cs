using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Basics
{
    public class Client
    {

        string eMessage = "No connection";
        static byte[] bytes = new byte[1024];
        public Socket sender;
        public IPAddress ipAddress;
        public IPEndPoint remoteEP;

        public string Socket()
        {
            try
            {
                sender = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                sender.Connect(remoteEP);
                return "Connected to server";
            }
            catch
            {
               return eMessage;
            }
        }

        public string Send(string message)
        {
            try
            {
                byte[] msg = Encoding.ASCII.GetBytes(message);
                sender.Send(msg);
                return "Command sent";
            }
            catch
            {
                return eMessage;
            }
        }

        public string Receive()
        {
            try
            {
                int bytesRec = sender.Receive(bytes);
                return Encoding.ASCII.GetString(bytes, 0, bytesRec);
            }
            catch
            {
                return eMessage;
            }

        }

        public string CloseConnection()
        {
            try
            {
                sender.Shutdown(SocketShutdown.Both);
                sender.Close();
                return "Connection closed";
            }
            catch
            {
                return eMessage;
            }
        }

        public string LightStatus()
        {
            Socket();
            Send("lStatus");
            string Lightstatus = Receive();
            CloseConnection();
            return Lightstatus;
        }

        public string SensorStatus()
        {
            Socket();
            Send("mStatus");
            string Sensorstatus = Receive();
            CloseConnection();
            return Sensorstatus;
        }

        public string TimerStatus()
        {
            Socket();
            Send("tStatus");
            string Timerstatus = Receive();
            CloseConnection();
            return Timerstatus;
        }
    }
}
