using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Basics
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Settings : ContentPage
    {
        bool BIsOn; //Changes when Movementsensor gets turned on or off
        Client client; //Makes a new client

        public Settings(Client client) //Constructor of Client
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false); //Turns off navigationbar on top of the app 
            IP.Text = Preferences.Get("IP_key", ""); //Collects the saved IP address
            port.Text = Preferences.Get("port_key", ""); //Collects the saved port
            TimerEntry.Text = Preferences.Get("timer_key", ""); //Collects saved timer length
            this.client = client; //Sets the client

        }

        private void Savebutton(object sender, EventArgs e) //Saves current IP and port
        {
            try
            {
                if (Convert.ToInt32(port.Text) > 65535 || Convert.ToInt32(port.Text) < 0) //Check to see if port is invalid
                {
                    port.Text = Convert.ToString(65535); //Converts input to valid port if input is invalid
                }
                Preferences.Set("IP_key", IP.Text); //Saves IP
                Preferences.Set("port_key", port.Text); //Saves port
            }
            catch
            {
            }

            try
            {
                if (Timer.IsVisible == true)
                {
                    client.Socket(); //Opens connection with server
                    client.Send("cTimer" + "," + TimerEntry.Text); //Sends message to server telling it to update timer length and new timer length
                    client.CloseConnection(); //Closes connection with server
                    Preferences.Set("timer_key", TimerEntry.Text); //Saves timer length
                }
            }
            catch (Exception)
            {

                throw;
            }

        }


        private void Clearbutton(object sender, EventArgs e) //Clears saved IP and port
        {
            Preferences.Clear(); //Clears saved IP and port

            DELETE.Text = "Saved settings deleted"; //Gives mesage to indicate to user that saved IP and port has been deleted


        }

        private void CColor(object sender, EventArgs e) //Button that opens custom color page
        {
            Navigation.PushAsync(new Custom(client)); //opens custom color page and creates a new client
        }

        private void BewegingssensorButton(object sender, EventArgs e) //Button that turns movementsensor on and off
        {
            if (BIsOn == true) //Check to see if movementsensor is on or off
            {
                try
                {
                    client.Socket(); //Opens connection with server
                    client.Send("sOff"); //Sends a message to server telling it to turn the sensor off
                    client.CloseConnection(); //Closes connection with server
                    Timer.IsVisible = false; //Turns entrybox for timer duration off
                    BIsOn = false; //Sets BIsOn to false
                    Bewegingssensor.TextColor = Color.Gray; //Changes button text color
                    Bewegingssensor.BackgroundColor = Color.FromHex("303030"); //Changes button background color
                }
                catch (Exception)
                {

                    throw;
                }
            }
            else
            {
                try
                {
                    client.Socket(); //Opens connection with server
                    client.Send("sOn"); //Sends a message to server telling it to turn the sensor on
                    client.CloseConnection(); //Closes connection with server
                    Timer.IsVisible = true; //Turns entrybox for timer duration on
                    BIsOn = true; //Sets BIsOn to true
                    Bewegingssensor.TextColor = Color.Black; //Changes button text color
                    Bewegingssensor.BackgroundColor = Color.White;//Changes button background color
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        protected void Connection(object sender, EventArgs e) //Navbar button that takes user to the Status page
        {
            Navigation.PushAsync(new Status(client)); //Takes user to the Status page
        }

        private void Lampjes(object sender, EventArgs e) //Navbar button that takes the user to the Lights page
        {
            Navigation.PushAsync(new Lights()); //Takes user to the Lights page
        }
    }
}
